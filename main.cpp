#include <KTimeZone>
#include <KCountry>

#include <QTimeZone>
#include <QMetaEnum>

#include <bits/ranges_algo.h>
#include <cstdlib>
#include <ktimezone.h>
#include <qlocale.h>
#include <unicode/tznames.h>
#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <unicode/localebuilder.h>

#include <iostream>
#include <string>

struct zoneinfo
{
    const char *id;
    icu::UnicodeString location;
    QLocale::Territory qt; 
    QLocale::Territory ki18n;
};

std::ostream &operator<<(std::ostream &os, QLocale::Territory country)
{
    static auto territoryEnum = QMetaEnum::fromType<QLocale::Territory>();
    return os << territoryEnum.valueToKey(country);
}

int main()
{

    const auto locale = icu::Locale(QLocale().name().toUtf8());
    UErrorCode error = U_ZERO_ERROR;
    const auto tzNames = icu::TimeZoneNames::createInstance(locale, error);
    if (!U_SUCCESS(error)) {
        std::cerr << "failed to initalize timezone names" << u_errorName(error);
    }

    auto ids = QTimeZone::availableTimeZoneIds();
    std::ranges::sort(ids);
    std::vector<zoneinfo> zones;
    zones.reserve(ids.count());

    std::cout << std::string(80, '#');
    std::cout << "\ntimezone\tcity\tcountry(Qt)\tcountry(KI18n)\n";
    icu::UnicodeString result;
    for (const auto& id : ids) {
        const auto &locationName = tzNames->getExemplarLocationName(icu::UnicodeString::fromUTF8(icu::StringPiece(id.data(), id.size())), result);
       zones.push_back({id.data(), locationName,  QTimeZone(id).territory(), KTimeZone::country(id).country()});
    }

    auto countriesMatch = [](const zoneinfo &zone) {
        return zone.qt == zone.ki18n;
    };
    auto hasCountry = [](const zoneinfo &zone) {
        return zone.qt != QLocale::AnyTerritory;
    };

    // Layout after partitioning
    // | location                                | no location                             |
    // | country | no country | country mismatch | country | no country | country mismatch |

    auto noLocation =  std::ranges::stable_partition(zones, std::not_fn(&icu::UnicodeString::isBogus), &zoneinfo::location);

    auto locationDisagreeingCountries = std::ranges::stable_partition(zones.begin(), noLocation.begin(),  countriesMatch);
    auto locationNoCountry = std::ranges::stable_partition(zones.begin(), locationDisagreeingCountries.begin(), hasCountry);

    auto noLocationDisagreeingCountries = std::ranges::stable_partition(noLocation.begin(), noLocation.end(),  countriesMatch);
    auto noLocationNoCountry = std::ranges::stable_partition(noLocation.begin(), noLocationDisagreeingCountries.begin(), hasCountry);

    std::cout << std::string(80, '#') << '\n';
    std::cout  << "Zones with location and country\n";
    std::cout << std::string(80, '#') << '\n'; 
    std::for_each(zones.begin(), locationNoCountry.begin(), [] (const zoneinfo &zone){
        std::cout << zone.id << '\t' << zone.location << '\t' << zone.qt << "\n";
    });
    std::cout << '\n' << std::string(80, '#') << '\n';
    std::cout  << "Zones with location but countries dont't match\n";
    std::cout << std::string(80, '#') << '\n';
    std::for_each(locationDisagreeingCountries.begin(), locationDisagreeingCountries.end(), [] (const zoneinfo &zone){
        std::cout << zone.id << '\t' << zone.location << '\t' << zone.qt << '\t' << zone.ki18n << "\n";
    });
    std::cout << '\n' << std::string(80, '#') << '\n';
    std::cout  << "Zones with location but with no country\n";
    std::cout << std::string(80, '#') << '\n';
     std::for_each(locationNoCountry.begin(), locationNoCountry.end(), [] (const zoneinfo &zone){
        std::cout << zone.id << '\t' << zone.location << '\t' << zone.qt << "\n";
    });

    std::cout << std::string(80, '#') << '\n';
    std::cout  << "Zones with no location but with country\n";
    std::cout << std::string(80, '#') << '\n'; 
    std::for_each(noLocation.begin(), noLocationNoCountry.begin(), [] (const zoneinfo &zone){
        std::cout << zone.id << '\t' << zone.location << '\t' << zone.qt << "\n";
    });
    std::cout << '\n' << std::string(80, '#') << '\n';
    std::cout  << "Zones with no locations and countries dont't match\n";
    std::cout << std::string(80, '#') << '\n';
    std::for_each(noLocationDisagreeingCountries.begin(), noLocationDisagreeingCountries.end(), [] (const zoneinfo &zone){
        std::cout << zone.id << '\t' << zone.location << '\t' << zone.qt << '\t' << zone.ki18n << "\n";
    });
    std::cout << '\n' << std::string(80, '#') << '\n';
    std::cout  << "Zones with no locations and  no country\n";
    std::cout << std::string(80, '#') << '\n';
     std::for_each(noLocationNoCountry.begin(), noLocationNoCountry.end(), [] (const zoneinfo &zone){
        std::cout << zone.id << '\t' << zone.location << '\t' << zone.qt << "\n";
    });
}
